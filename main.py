#!/bin/python3

import sympy
import os
import numpy as np
from control import *
from numpy.linalg import inv
from sympy import symbols, pprint, Float
from sympy.matrices import Matrix, zeros
from sympy.physics.mechanics import dynamicsymbols

(width, _) = os.get_terminal_size(0)

#===============================================================================

parameters = {
    'La': 0.2,
    'Lp': 0.3,
    'Ia': 0.2 * 0.2 ** 2,
    'Ie': 0.0003,
    'mp': 0.25,
    'Ca': 0.5,
    'Cp': 0.0001,
    'g': 9.81
}

#===============================================================================

(La, Lp, Ia, Ie, mp, Ca, Cp, g) = symbols('La Lp Ia Ie mp Ca Cp g', real=True)
(AL0, AL1, AL2, TH0, TH1, TH2) = symbols('AL0 AL1 AL2 TH0 TH1 TH2', real=True)

#===============================================================================

al0 = dynamicsymbols('al')
al1 = dynamicsymbols('al', 1)
al2 = dynamicsymbols('al', 2)

th0 = dynamicsymbols('th')
th1 = dynamicsymbols('th', 1)
th2 = dynamicsymbols('th', 2)

u = dynamicsymbols('u')

#===============================================================================

l = dynamicsymbols('l')

v_sq = (La ** 2 + l ** 2 * sympy.sin(th0) ** 2) * al1 ** 2 +\
        Lp ** 2 * th1 ** 2 +\
        2 * Lp * La * th1 * al1 * sympy.cos(th0)

dEk = v_sq * mp / (2 * Lp)
Ekp = sympy.integrate(dEk, (l, 0, Lp))

# Ekp = (mp / 2) * ((La * al1 + Lp * th1 * sympy.cos(th0)) ** 2 + (Lp * th1 * sympy.sin(th0)) ** 2)

#===============================================================================

Eka = Ia * al1 ** 2 / 2
Eke = Ie * th1 ** 2 / 2

Epp = mp * Lp * g * (1 + sympy.cos(th0))

#===============================================================================

Ek = Eka + Ekp # + Eke
Ep = Epp

L = Ek - Ep

#===============================================================================

Mcp = Cp * th1;
Mca = Ca * al1;

dL_th = sympy.diff(L, th0) - sympy.diff(sympy.diff(L, th1), 't')
dL_al = sympy.diff(L, al0) - sympy.diff(sympy.diff(L, al1), 't')

Mp = dL_th + Mcp
Ma = dL_al + Mca - u

#===============================================================================

def simplify_pprint(var):
    print("\n")
    print("=" * width)
    print("\n")
    pprint(sympy.simplify(var.subs(al2, AL2).subs(al1, AL1)
                          .subs(th2, TH2).subs(th1, TH1).subs(th0, TH0)))


def linearize_full(var):
    linvar = var + sympy.diff(var, th0) * (th0 - 0) + sympy.diff(var, al0) * (al0 - 0)
    return (linvar.subs(th2, TH2).subs(th1, TH1).subs(al2, AL2).subs(al1, AL1)
            .subs(th0, 0).subs(al0, 0).subs(TH2, th2).subs(TH1, th1)
            .subs(AL2, al2).subs(AL1, al1))


def linearize(var):
    return (var.subs(sympy.sin(th0), th0)
            .subs(sympy.cos(th0), 1)
            .subs(th0 ** 2, 0)
            .subs(th1 ** 2, 0)
            .subs(th0 * th1, 0)
            .subs(al1 ** 2, 0))


def evaluate_raw(var):
    return (var.subs(La, parameters["La"])
            .subs(Lp, parameters["Lp"])
            .subs(Ia, parameters["Ia"])
            .subs(Ie, parameters["Ie"])
            .subs(mp, parameters["mp"])
            .subs(Ca, parameters["Ca"])
            .subs(Cp, parameters["Cp"])
            .subs(g, parameters["g"]))


def evaluate(var):
    return evaluate_raw(linearize(var))

#===============================================================================

simplify_pprint(v_sq)
simplify_pprint(Ekp)
simplify_pprint(L)
simplify_pprint(dL_th)
simplify_pprint(dL_al)
simplify_pprint(Ma)
simplify_pprint(Mp)
simplify_pprint(linearize(Ma))
simplify_pprint(linearize(Mp))
print("\n")
print("#" * width)
simplify_pprint(evaluate_raw(Ma))
simplify_pprint(evaluate_raw(Mp))
print("\n")
print("*" * width)
simplify_pprint(evaluate(Ma))
simplify_pprint(evaluate(Mp))
print("\n")
print("*" * width)

coeffs = [[Float(evaluate(m).collect(s).coeff(s).evalf(), 10) for m
           in [Mp, Ma]] for s in [th0, al0, th1, al1, th2, al2, u]]

D0 = Matrix([coeffs[0], coeffs[1]])
D1 = Matrix([coeffs[2], coeffs[3]])
D2 = Matrix([coeffs[4], coeffs[5]])
U0 = Matrix([coeffs[6]]).T

S1 = (D2**-1) * D1
S0 = (D2**-1) * D0

S = S0.col_insert(2, S1)
U = (D2**-1) * U0

pprint(S)
pprint(U)

print("\n")
print("*" * width)

A = (S.row_insert(0, Matrix([[0, 0, 1, 0]]))
     .row_insert(1, Matrix([[0, 0, 0, 1]])))

B = U.row_insert(0, Matrix([[0]])).row_insert(1, Matrix([[0]]))

pprint(A)
pprint(B)

print("\n")
print("*" * width)

Q = sympy.zeros(4, 4)
Q[0, 0] = 20
Q[1, 1] = 2
Q[2, 2] = 50
Q[3, 3] = 10

R = sympy.eye(1)

pprint(Q)
pprint(R)

print("\n")
print("*" * width)

K, S, E = lqr(A, B, Q, R)
pprint(K)
print("-----------")

#pprint(S)
#pprint(E)
